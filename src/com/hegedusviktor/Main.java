package com.hegedusviktor;
public class Main {

    public static void main(String[] args) {
        Airplane airplane = new Airplane();
        final int MAX_AGE = 99;
        final int MIN_AGE = 30;

        while (true) {
            try {
                int weight = (int) (Math.random() * (MAX_AGE - MIN_AGE)) + MIN_AGE;
                Passenger passenger = new Passenger(weight);
                airplane.fill(passenger);
                System.out.println(passenger);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                break;
            }
        }

        AirplaneShow.showSeatingArrangement(airplane);
    }
}
