package com.hegedusviktor;

public class Passenger {
    private final int weight;
    private int row;
    private int column;
    private static final int ADULT_WEIGHT = 40;
    private final boolean adult;
    private String serialNumber;

    public Passenger(int weight) {
        this.weight = weight;
        this.adult = ADULT_WEIGHT <= this.weight;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isAdult() {
        return adult;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "weight=" + weight +
                ", row=" + row +
                ", column=" + column +
                ", adult=" + adult +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }
}
