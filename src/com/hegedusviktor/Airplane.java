package com.hegedusviktor;

public class Airplane {
    private final int[][] seats;
    private final Passenger[][] passengers;
    private int passengerCount;
    private boolean top;
    private int sumWeight;
    private static final int MAX_CAPACITY = 180;
    private static final int MAX_WEIGHT = 180 * 60;

    public Airplane() {
        seats = new int[30][6];
        passengers = new Passenger[30][6];
        passengerCount = 0;
        top = true;
        sumWeight = 0;
    }

    public void fill(Passenger passenger) throws Exception {
        if (passenger == null) throw new Exception("Invalid passenger was added.");
        if (passenger.getWeight() <= 0) throw new Exception("Weight cannot be less then zero.");
        if (capacityReached()) throw new Exception("Capacity reached, cannot add new passenger.");
        if (maxWeightReached()) throw new Exception("Maximum weight reached, cannot add new passenger.");

        if (top) fillFromTop(passenger);
        else fillFromBottom(passenger);

        top = !top;
        passengerCount++;
        sumWeight += passenger.getWeight();
    }

    private void fillFromTop(Passenger passenger) {
        emptySeatFound:
        for (int i = 0; i < seats.length; i++) {
            for (int j = 0; j < seats[i].length; j++) {
                if (seats[i][j] == 0) {
                    bookSeatForPassenger(passenger, i, j);
                    break emptySeatFound;
                }
            }
        }
    }

    private void fillFromBottom(Passenger passenger) {
        emptySeatFound:
        for (int i = seats.length - 1; i >= 0; i--) {
            for (int j = seats[i].length - 1; j >= 0; j--) {
                if (seats[i][j] == 0) {
                    bookSeatForPassenger(passenger, i, j);
                    break emptySeatFound;
                }
            }
        }
    }

    private void bookSeatForPassenger(Passenger passenger, int i, int j) {
        seats[i][j] = passenger.isAdult() ? 1 : -1;
        passenger.setRow(i);
        passenger.setColumn(j);
        passenger.setSerialNumber(i + "" + j);
        passengers[i][j] = passenger;
    }

    private boolean capacityReached() {
        return passengerCount == MAX_CAPACITY;
    }

    private boolean maxWeightReached() {
        return MAX_WEIGHT <= sumWeight;
    }

    public int[][] getSeats() {
        return seats;
    }

    public Passenger[][] getPassengers() {
        return passengers;
    }
}
