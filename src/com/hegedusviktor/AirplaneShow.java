package com.hegedusviktor;

public class AirplaneShow {
    private final static int MAX_SINGLE_DIGIT = 9;

    public static void showSeatingArrangement(Airplane airplane) {
        int[][] seats = airplane.getSeats();
        Passenger[][] passengers = airplane.getPassengers();
        System.out.println();

        for (int i = 0; i < seats.length; i++) {
            String rowSpace = getSpaces(i);
            System.out.print("#" + (i + 1) + rowSpace);

            for (int j = 0; j < seats[i].length; j++) {
                Passenger passenger = passengers[i][j];
                String whitespace = seats[i][j] == -1 ? "   " : "    ";
                String weight = passenger == null ? "00" : String.valueOf(passenger.getWeight());
                System.out.print(whitespace + seats[i][j] + " (" + weight + " kg)");
            }
            System.out.println();
        }
    }

    private static String getSpaces(int i) {
        return i < MAX_SINGLE_DIGIT ? "  |   " : " |   ";
    }
}
